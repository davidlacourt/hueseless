const mdns = require('mdns');

const sequence = [
  mdns.rst.DNSServiceResolve(),
  'DNSServiceGetAddrInfo' in mdns.dns_sd ? mdns.rst.DNSServiceGetAddrInfo() : mdns.rst.getaddrinfo({families:[4]}),
  mdns.rst.makeAddressesUnique(),
];

const browser = mdns.createBrowser(mdns.tcp('googlecast'), {resolverSequence: sequence});

async function search(waitTime = 1000) {

  let services = []

  browser.start()
  browser.on('serviceUp', service => { services.push(service) })
  await new Promise(resolve => setTimeout(resolve, waitTime))
  browser.stop()

  return services
    .filter(service => {
      console.log(`service: ${service}`);
      if (service.txtRecord && service.txtRecord.md && service.txtRecord.md.match && service.txtRecord.md.match(/^Google Home/)) {
        return service;
      }
    })
    .map(service => {
      return Object.assign(service.txtRecord, { address: service.addresses[0] });
    })
}

module.exports = search
