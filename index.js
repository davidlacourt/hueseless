const fetch = require('node-fetch');
const { timer }  = require('rxjs');
const { map } = require('rxjs/operators');
const Connecter = require('./connect');
let verbose = false;

const TEXT = {
  'fr-FR': {
    'light.monitoring.enabled': 'Surveillance de la lumière activée!',
    'light.turned.off': 'J\'ai éteint la lumière',
  },
  'en-US': {
    'light.monitoring.enabled': 'Light status monitoring enabled!',
    'light.turned.off': 'I turned off the lights',
  }
};

function getVERBOSEFromEnvVarsElseFalse() {
  if (!process.env['VERBOSE']) {
    console.error('You could specify VERBOSE if you want more logs');
    return false;
  } else {
    console.debug('VERBOSE is ' + process.env['VERBOSE']);
    return true; 
  }
}

function getHUESERFromEnvVarsOrRageQuit() {
  if (!process.env['HUESER']) {
    console.error('This needs a HUESER, look at the README.md for how to get one.');
    process.exit(1);
  } else {
    console.debug('HUESER is ' + process.env['HUESER']);
    return process.env['HUESER'];
  }
}

function getLangEnvVarsOrDefaultsTo(defaultLang = 'en-US') {
  if (!process.env['GOOGLE_HOME_LANG']) {
    console.error(`This might need a GOOGLE_HOME_LANG, but it will default to ${defaultLang}. look at the README.md for how to get one.`);
    return defaultLang;
  } else {
    console.debug('GOOGLE_HOME_LANG is ' + process.env['GOOGLE_HOME_LANG']);
    return process.env['GOOGLE_HOME_LANG'];
  }
}

function getTargetHueLightIdFromEnvVarsOrRageQuit() {
  if (!process.env['TARGET_HUE_LIGHT_ID']) {
    console.error('This needs a TARGET_HUE_LIGHT_ID, look at the README.md for how to get one.');
    process.exit(1);
  } else {
    console.debug('TARGET_HUE_LIGHT_ID is ' + process.env['TARGET_HUE_LIGHT_ID']);
    return process.env['TARGET_HUE_LIGHT_ID'];
  }
}

async function fetchBridgeIPAddressOrRageQuit() {
  try {
    const res = await fetch(`https://discovery.meethue.com/`);
    const data = await res.json();
    //console.debug(data);
    console.debug('bridgeIPAdress : ' + data[0].internalipaddress);
    return data[0].internalipaddress;
  } catch (e) {
    console.error('woops, something went wrong trying to discover the Hue Bridge IP');
    console.error(e);
    process.exit(1);
  }
}

async function fetchLightStatesOrRageQuit({ hueBridgeIPAdress, hueser }) {
  const url = `http://${hueBridgeIPAdress}/api/${hueser}/lights`;
  const res = await fetch(url);
  const data = await res.json();
  //console.debug(data);
  if (data.length && data.length > 0 && data[0].error) {
    console.error(`error fetching ${url}: ${data[0].error.description}`);
    process.exit(1);
  }
  return data;
}

async function shutDownTheDamnLight({ hueBridgeIPAdress, hueser, targetHueLightId }) {
  const url = `http://${hueBridgeIPAdress}/api/${hueser}/lights/${targetHueLightId}/state`;
  const res = await fetch(url, { method: 'PUT', body: '{"on":false}' });
  const data = await res.json();
  //console.debug(data);
  if (data.length && data.length > 0 && data[0].error) {
    console.error(`error fetching ${url}: ${data[0].error.description}`);
    process.exit(1);
  }
  if (data.length && data.length > 0 && data[0].success) {
    console.log(`success!`);
    return data[0].success;
  }
  return data;
}

async function loop({ index, hueBridgeIPAdress, hueser, targetHueLightId }) {
  verbose && console.log(`this is loop nb ${index}`);
  const state = await fetchLightStatesOrRageQuit({ hueBridgeIPAdress, /*"""*/ hueser /*"""*/ });
  if (verbose) {
    const nbLights = Object.keys(state).length;
    console.log(`you have ${nbLights} lights`);
    console.log(
      Object.keys(state)
        .map(k => `light ${k} is ${state[k].state.on ? 'ON' : 'OFF'}`)
        .join('\n'),
    );
    console.log(`----`);
  }
  const data = state[targetHueLightId].state;
  verbose && console.log(data);
  return data;
}

async function searchGoogleHomeIpIfExistElseUndefined() {
  const search = require('./search-google-home');
  try {
    const found = await search();
    console.log(found);
    return found;
  } catch (e) {
    console.error(e);
    return undefined;
  }
}

async function initGoogleHome({ ip, lang = 'en-US' }) {
  let device = new Connecter(ip, lang);
  device.config({ lang });
  // If you do this, google home will immediately sound when you call speak() or playMedia()
  try {
    await device.readySpeaker();
  } catch (e) {
    console.error('error trying to ready speaker');
    console.error(e);
  }
  return device;
}
async function saySomethingThroughGoogleHome(device, msg) {
  try {
    console.log(await device.speak(msg));
  } catch (e) {
    console.error('error trying to speak');
    console.error(e);
  }
}

(async function main() {
  const /*"""*/hueser/*"""*/ = getHUESERFromEnvVarsOrRageQuit();
  const hueBridgeIPAdress = await fetchBridgeIPAddressOrRageQuit();
  const targetHueLightId = await getTargetHueLightIdFromEnvVarsOrRageQuit();
  const googleHomeLang = await getLangEnvVarsOrDefaultsTo('en-US');
  const foundGoogleHome = await searchGoogleHomeIpIfExistElseUndefined();
  verbose = getVERBOSEFromEnvVarsElseFalse();
  let device = undefined;
  if (foundGoogleHome && foundGoogleHome.length>0) {
    console.log(`found a Google Home named '${foundGoogleHome[0].fn}' with IP ${foundGoogleHome[0].address}`);
    device = await initGoogleHome({ ip: foundGoogleHome[0].address, lang: googleHomeLang });
    await saySomethingThroughGoogleHome(device, TEXT[googleHomeLang]['light.monitoring.enabled']);
  }

  const source = timer(1000, 2000);
  const subscribed = source.pipe(
    map(val => loop({ index: val, hueBridgeIPAdress, hueser, targetHueLightId })),
    // I did not manage to use a magic RxJs operator to detect changes on the state value...
  ).subscribe(async state => {
    const data = await state;
    //console.log('detected state change!');
    //console.log(data);
    if (data) {
      if (data.on) {
        await shutDownTheDamnLight({ hueBridgeIPAdress, hueser, targetHueLightId });
        console.log(`Light ${targetHueLightId} was ON, so I shut it down!`);

        if (device) {
          await saySomethingThroughGoogleHome(device, TEXT[googleHomeLang]['light.turned.off']);
        }

      } else {
        //console.log('Light was already OFF');
      }
    } else {
      console.log('state is empty');
    }
  });

})().then(() => { console.log('DONE!') });

