# HUEseless

![Like this one from ThinkGeek](https://www.thinkgeek.com/images/products/zoom/ivlq_useless_light_switch.gif)
(This is a physical one from [ThinkGeek](https://www.thinkgeek.com/product/ivlq/))

This project uses [Philips HUE APIs](https://developers.meethue.com/develop/) to make a silly side-project.

## My genius evil plan

![Dr Evil petting his cat](https://media.giphy.com/media/MgwVy8YxMqJ56/giphy.gif)

The story is:

Back then, I installed a HUE switch in my kids' bedroom.

As soon as I closed their door when time to sleep has had come, they would get up and turn it back on!

Then a war of turning on & off lights ensued.

As a geek dad, I want to automate this into an evil AI, so that I can laugh madly while petting my cat, mwahahahahah!

## How to install & run

You need to follow Philips HUE instructions to create a user.
Here's how I did it, it might not work for you if you're reading this too far in the future.

- Captain Obvious says: "You need to have at least one Philips Hue light and a Hue Bridge, that are plugged in, linked to you local WiFi network, and be able to use the Hue app from your phone"
- Then, be sure you are on the same WiFi network as your Hue Bridge; this whole thing only works locally,
- go to the Hue Discovery website : https://discovery.meethue.com/
- copy the IP address
- go to this http(s)://{IP of bridge}/debug/clip.html
- go click on the bridge big button, then in your browser :
- make a `POST` to `/api` with a body `{"devicetype":"my_hue_app#iphone drevil"}`
- it should return a new auth user: `h9qjfdOohk8fTY6dv7tm-TGmqdfrHsE5fpS-jW3QY` (this is not a valid user, sorry)
- then `GET` `/api/{this id}` should show a JSON payload with all the status info of your Hue gear.
- use this... """HUESER""" ![dr evil quoting](https://media.giphy.com/media/qs6ev2pm8g9dS/giphy.gif) as an environment variable to run the App, like so:

```bash
npm i
HUESER=h9qjfdOohk8fTY6dv7tm-TGmqdfrHsE5fpS-jW3QY TARGET_HUE_LIGHT_ID=3 npm start
```

Where `TARGET_HUE_LIGHT_ID` is the ID of one of your lights that you want to always be OFF.

## Google Home progressive enhancement

The App also searches for a Google Home to use it as its mouth to say stuff.

You can provide a custom language for what it will say using another environment variable: `GOOGLE_HOME_LANG`.
Only two are currently supported: `fr-FR` and `en-US`.

Currently, I include the files and dependencies of the `node-googlehome` repository, but I made a PR and should it be merged, I will be able to remove this.

## Problem encountered running this on a Raspberry Pi Zero

I played around with a RPi0 to run this server without my full computer being on. And also just to find a use for the Raspberry Pi Zero.

You'll have to install Avahi support for Bonjour:

```bash
sudo apt-get install libavahi-compat-libdnssd-dev
```

Also had an error about "getaddrinfo -3008", and found a fix in [this GitHub Issue for node-mdns](https://github.com/agnat/node_mdns/issues/130)

For future reference, I was thus able to run it from a MacOS environment and from a Raspbian on a Raspberry Pi Zero+

## Next

I tried to make a smart use of RxJs to detect change of state, but did not manage to. If someone can help me mix `async-await` with `distinctUntilChanged`, your PR will be welcome!

I could also containerize it to run it within a Docker image. But I have found errors for the Bonjour library which messes up between MacOS and Linux over MacOS through Docker... So I did not complete this part. Instead I put my Raspberry Pi Zero to good use.

Have the program guide you to configure things instead of doing all of this manually (interactive mode).
Also, list all Lights, and make you choose which one you want to turn off.


